<?php

namespace StyleWishApi\Objects;

use StyleWishApi\Client as ApiClient;
use StyleWishApi\Collections\FeedItems;
use StyleWishApi\Collections\FeedProducts;

class FeedItem extends AbstractObject
{
    protected $collection;
    protected $products;

    public function __construct(array $data, ApiClient $api, FeedItems $collection = null)
    {
        parent::__construct($data, $api);
        $this->collection = $collection;
    }

    public function offsetGet($key)
    {
        if ($key === 'site') {
            if ($this->offsetExists('site')) {
                return parent::offsetGet('site');
            }
            return $this->collection->getSite(parent::offsetGet('site_id'));
        }
        if ($key === 'products') {
            if (!isset($this->products)) {
                $p = parent::offsetGet($key);
                try {
                    $this->products = new FeedProducts(['products' => $p], $this->apiClient);
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            }
            return $this->products;
        }
        if ($key === 'feed') {
            return $this->collection->getFeed();
        }
        return parent::offsetGet($key);
    }
}
