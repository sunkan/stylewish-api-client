<?php

namespace StyleWishApi\Objects;

use StyleWishApi\Client as ApiClient;

class FeedCollection extends AbstractObject
{
    public function getItems($query, $offset = 0, $limit = 50)
    {
        return $this->apiClient->feed->getItems($this['_id'], $query, $offset, $limit);
    }
}
