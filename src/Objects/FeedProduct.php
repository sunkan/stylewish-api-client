<?php

namespace StyleWishApi\Objects;

use StyleWishApi\Client as ApiClient;
use StyleWishApi\Collections\FeedProducts;

class FeedProduct extends AbstractObject
{
    protected $collection;

    public function __construct(array $data, ApiClient $api, FeedProducts $collection)
    {
        parent::__construct($data, $api);
        $this->collection = $collection;
    }
    public function offsetGet($key)
    {
        return parent::offsetGet($key);
    }
}
