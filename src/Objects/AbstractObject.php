<?php

namespace StyleWishApi\Objects;

use ArrayObject;
use StyleWishApi\Client as ApiClient;

abstract class AbstractObject extends ArrayObject
{
    protected $apiClient;

    public function __construct(array $data, ApiClient $api)
    {
        parent::__construct($data);
        $this->apiClient = $api;
    }
}
