<?php

namespace StyleWishApi\Controller;

use StyleWishApi\Client as ApiClient;

class Likes
{
    protected $apiClient;

    public function __construct(ApiClient $api)
    {
        $this->apiClient = $api;
    }

    public function unlike($type, $uuid)
    {
        $response = $this->apiClient->call('DELETE', 'likes/' . $type . '/' . $uuid);

        return $response['like'];
    }

    public function like($type, $uuid)
    {
        $params = [
            'key' => $uuid,
        ];
        $response = $this->apiClient->call('POST', 'likes/' . $type, $params);

        return $response['like'];
    }

    public function byType($type, $includeObjects = false, $offset = 0, $limit = 50)
    {
        $params = [
            'include_objects' => $includeObjects,
            'offset' => $offset,
            'limit' => $limit,
        ];
        $response = $this->apiClient->call('GET', 'likes/' . $type, $params);

        if ($includeObjects) {
            return $response[$type];
        }
        return $response['likes'];
    }

    public function byTypeAndId($type, $id)
    {
        $response = $this->apiClient->call('GET', 'likes/' . $type . '/' . $id);

        return $response['like'];
    }

    public function all()
    {
        return $this->apiClient->call('GET', 'likes/');
    }
}
