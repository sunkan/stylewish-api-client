<?php

namespace StyleWishApi\Controller;

use StyleWishApi\Client as ApiClient;

class Accounts
{
    protected $apiClient;

    public function __construct(ApiClient $api)
    {
        $this->apiClient = $api;
    }

    public function create($provider, array $data)
    {
        $payload = [
            'device_id' => '5d7fb998-e55a-489f-899f-f6b5918fe841',
            'device_system' => 'stylewish-client',
            'provider' => $provider,
        ];
        $payload['account'] = $data;

        $response = $this->apiClient->call('post', 'accounts/', $payload);
        return $response['account'];
    }
}
