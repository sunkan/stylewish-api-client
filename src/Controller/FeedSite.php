<?php

namespace StyleWishApi\Controller;

use Exception;
use StyleWishApi\Client as ApiClient;
use StyleWishApi\Collections;
use StyleWishApi\Objects;

class FeedSite
{
    protected $apiClient;

    public function __construct(ApiClient $api)
    {
        $this->apiClient = $api;
    }

    public function all($search = false, $offset = 0, $limit = 50, array $params = [])
    {
        $params['offset'] = $offset;
        $params['limit'] = $limit;

        if ($search) {
            $params['search'] = $search;
        }
        $response = $this->apiClient->call('GET', 'feed_sites/', $params);

        if ($response['sites']) {
            return new Collections\FeedSites($response, $this->apiClient);
        }
    }

    public function followers($siteId)
    {
        $response = $this->apiClient->call('GET', 'feed_sites/' . $siteId . '/followers', [
            'only_collections' => 0,
        ]);
        $collections = [];
        foreach ($response['followers'] as $follower) {
            $collections[] = new Objects\FeedCollection($follower, $this->apiClient);
        }
        return $collections;
    }

    public function items($id, $query, $offset = 0, $limit = 100)
    {
        $query['offset'] = $offset;
        $query['limit'] = $limit;

        $response = $this->apiClient->call('GET', 'feed_sites/' . $id . '/items', $query);
        $response['sites'][$id] = $response['site'];
        if ($response['items']) {
            return new Collections\FeedItems($response, $this->apiClient);
        }
        throw new Exception('No items found');
    }

    public function get($id)
    {
        $response = $this->apiClient->call('GET', 'feed_sites/' . $id);
        if ($response['site']) {
            return new Objects\FeedSite($response['site'], $this->apiClient);
        }
        throw new Exception('Unkown error');
    }
}
