<?php

namespace StyleWishApi\Controller;

use Exception;
use StyleWishApi\Client as ApiClient;

class Auth
{
    protected $apiClient;

    public function __construct(ApiClient $api)
    {
        $this->apiClient = $api;
    }

    public function validate($token)
    {
        $payload = [
            'access_token' => $token,
        ];

        $this->apiClient->call('post', 'auth/validate', $payload);
        return true;
    }

    public function renewAccessToken()
    {
        $response = $this->apiClient->call('get', 'auth/renew');
        return $response['access_token'];
    }

    public function logout()
    {
        $this->apiClient->call('get', 'auth/logout');
        return true;
    }

    public function login($provider, array $data)
    {
        $payload = [
            'device_id' => '5d7fb998-e55a-489f-899f-f6b5918fe841',
            'device_system' => 'stylewish-client',
            'provider' => $provider,
        ];
        $payload[$provider] = $data;

        $response = $this->apiClient->call('post', 'auth/login', $payload);
        if ($response['account']) {
            return $response['account'];
        }
        throw new Exception('Unkown error');
    }
}
