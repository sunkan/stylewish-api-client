<?php

namespace StyleWish\Api;

use StyleWishApi\Controller as ApiClient;

class Shop
{
    protected $apiClient;

    public function __construct(ApiClient $api)
    {
        $this->apiClient = $api;
    }

    public function getGenders($store = null)
    {
        $path = 'shop/genders';
        if ($store) {
            $path .= '/' . $store;
        }

        $data = $this->apiClient->call('GET', $path);

        return $data['genders'];
    }

    public function getCategorys($store = null)
    {
        $path = 'shop/categories';
        if ($store) {
            $path = 'shop/store/' . $store . '/categories';
        }

        $data = $this->apiClient->call('GET', $path);

        return $data['categories'];
    }

    public function getAccounts()
    {
        $data = $this->apiClient->call('GET', 'shop/accounts');

        return $data['accounts'];
    }

    public function getCampaigns()
    {
        $data = $this->apiClient->call('GET', 'shop/campaigns');

        return $data['campaigns'];
    }

    public function getProducts(array $filter, $offset = 0, $limit = 100)
    {
        $filter['offset'] = (int) $offset;
        $filter['limit'] = (int) $limit;
        $data = $this->apiClient->call('GET', 'shop/products', $filter);
        return $data['products'];
    }
}
