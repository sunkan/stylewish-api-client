<?php

namespace StyleWishApi\Controller;

use Exception;
use StyleWishApi\Client as ApiClient;
use StyleWishApi\Collections;

class FeedProducts
{
    protected $apiClient;

    public function __construct(ApiClient $api)
    {
        $this->apiClient = $api;
    }
    public function byItemId($id)
    {
        $response = $this->apiClient->call('GET', 'feed/item/' . $id . '/products');

        if ($response['products']) {
            return new Collections\FeedProducts($response, $this->apiClient);
        }
        throw new Exception('Unkown error');
    }
}
