<?php

namespace StyleWishApi\Controller;

use Exception;
use StyleWishApi\Client as ApiClient;
use StyleWishApi\Objects;

class FeedCollection
{
    protected $apiClient;

    public function __construct(ApiClient $api)
    {
        $this->apiClient = $api;
    }

    public function create($name, array $sites)
    {
        $response = $this->apiClient->call('POST', 'feed/', [
            'name' => $name,
            'sites' => $sites,
        ]);
        if ($response['feed']) {
            return new Objects\FeedCollection($response['feed'], $this->apiClient);
        }
        throw new Exception('Unkown error');
    }

    public function get($id)
    {
        $response = $this->apiClient->call('GET', 'feed/' . $id);

        if ($response['feed']) {
            return new Objects\FeedCollection($response['feed'], $this->apiClient);
        }
        throw new Exception('Unkown error');
    }

    public function update($id, $data)
    {
        if (!isset($data['_id'])) {
            $data['_id'] = $id;
        }
        $response = $this->apiClient->call('PUT', 'feed/' . $id, $data);

        return true;
    }

    public function addSite($collectionId, $siteId)
    {
        $payload = [
            [
                'op' => 'add',
                'path' => '/sites/0',
                'value' => [
                    '_id' => $siteId,
                ],
            ],
        ];
        $this->apiClient->call('PUT', 'feed/' . $collectionId, $payload);

        return true;
    }

    public function deleteSite($collectionId, $index)
    {
        $payload = [
            [
                'op' => 'remove',
                'path' => '/sites/' . $index,
            ],
        ];
        $this->apiClient->call('PUT', 'feed/' . $collectionId, $payload);

        return true;
    }
}
