<?php

namespace StyleWishApi\Controller;

use Exception;
use StyleWishApi\Client as ApiClient;
use StyleWishApi\Collections;
use StyleWishApi\Objects;

class Feed
{
    protected $apiClient;

    public function __construct(ApiClient $api)
    {
        $this->apiClient = $api;
    }

    public function getPublicFeeds()
    {
        $response = $this->apiClient->call('GET', 'feed/');
        if ($response['feeds']) {
            $feeds = [];
            foreach ($response['feeds'] as $feed) {
                $feeds[] = new Objects\FeedCollection($feed, $this->apiClient);
            }
            return $feeds;
        }
        throw new Exception('Unkown error');
    }

    public function getItems($id, array $query = [], $offset = 0, $limit = 50)
    {
        $query['limit'] = (int) $limit;
        $query['offset'] = (int) $offset;
        $response = $this->apiClient->call('GET', 'feed/' . $id . '/items', $query);

        if ($response['items']) {
            return new Collections\FeedItems($response, $this->apiClient);
        }
        throw new Exception('Unkown error');
    }
}
