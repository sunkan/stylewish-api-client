<?php

namespace StyleWishApi\Controller;

use Exception;
use StyleWishApi\Client as ApiClient;
use StyleWishApi\Collections;
use StyleWishApi\Objects;

class FeedItem
{
    protected $apiClient;

    public function __construct(ApiClient $api)
    {
        $this->apiClient = $api;
    }
    public function get($id, array $include = [])
    {
        $params = [];
        if (count($include)) {
            foreach ($include as $key) {
                $params['include_' . $key] = 1;
            }
        }
        $response = $this->apiClient->call('GET', 'feed/item/' . $id, $params);

        if ($response['item']) {
            return new Objects\FeedItem($response['item'], $this->apiClient, null, $response['site']);
        }
        throw new Exception('Unkown error');
    }

    public function search($tag, $offset = 0, $limit = 100)
    {
        $params = [
            'search' => $tag,
            'offset' => $offset,
            'limit' => $limit,
        ];
        $response = $this->apiClient->call('GET', 'feed/items/', $params);

        if ($response['items']) {
            return new Collections\FeedItems($response, $this->apiClient);
        }
        throw new Exception('No items found');
    }
}
