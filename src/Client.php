<?php

namespace StyleWishApi;

use DomainException;
use RuntimeException;
use UnexpectedValueException;

class Client
{
    protected $controllerCache = [];
    protected $id;
    protected $accessToken = false;
    protected $token;
    protected $curl;
    protected $baseUrl = 'https://api.stylewish.me/';

    public function __construct($id, $token)
    {
        $this->id = $id;
        $this->token = $token;
        $this->curl = new Curl;
    }

    public function setAccessToken($token)
    {
        $this->accessToken = $token;
    }

    public function __get($key)
    {
        if (isset($this->controllerCache[$key])) {
            return $this->controllerCache[$key];
        }

        $instanceName = 'StyleWishApi\\Controller\\' . ucfirst($key);

        $class = new $instanceName($this);

        if ($class) {
            $this->controllerCache[$key] = $class;
        } else {
            $this->controllerCache[$key] = false;
        }
        return $this->controllerCache[$key];
    }

    public function call($method, $path, array $args = [])
    {
        $this->curl->reset();
        $this->curl->setopt(CURLOPT_RETURNTRANSFER, true);
        $this->curl->setHeader('X-Api-Id', $this->id);
        $this->curl->setHeader('X-Api-Token', $this->token);
        if ($this->accessToken) {
            $this->curl->setHeader('X-Api-Access', $this->accessToken);
        }
        $reqMethod = strtolower($method);
        $this->curl->$reqMethod($this->baseUrl . $path, $args);

        $response = $this->curl->response;
        $json = json_decode($response, true);
        if ($json) {
            if ($json['status'] === 'success') {
                return $json['data'];
            }
            if ($json['message'] || $json['data']['message']) {
                throw new DomainException($json['message'] ?: $json['data']['message'], $json['code'] ?: 0);
            }
            throw new DomainException($json['status']);
        }
        if ($this->curl->error) {
            throw new RuntimeException($this->curl->error_message, $this->curl->error_code);
        }
        throw new UnexpectedValueException($response);
    }
}
