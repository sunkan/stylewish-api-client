<?php

namespace StyleWishApi\Collections;

use ArrayIterator;
use StyleWishApi\Client as ApiClient;
use StyleWishApi\Objects\FeedSite;

class FeedSites extends ArrayIterator
{
    protected $apiClient;
    protected $cache = [];

    public function __construct(array $response, ApiClient $api)
    {
        $this->apiClient = $api;
        parent::__construct($response['sites']);
    }

    public function current()
    {
        $item = parent::current();
        if (!isset($this->cache[$item['_id']])) {
            $this->cache[$item['_id']] = new FeedSite($item, $this->apiClient, $this);
        }
        return $this->cache[$item['_id']];
    }
}
