<?php

namespace StyleWishApi\Collections;

use ArrayIterator;
use StyleWishApi\Client as ApiClient;
use StyleWishApi\Objects\FeedCollection;
use StyleWishApi\Objects\FeedItem;

class FeedItems extends ArrayIterator
{
    protected $apiClient;
    protected $sites;
    protected $feed;
    protected $cache = [];

    public function __construct(array $response, ApiClient $api)
    {
        $this->apiClient = $api;
        $this->sites = $response['sites'];
        if (isset($response['feed'])) {
            $this->feed = new FeedCollection($response['feed'], $api);
        }
        parent::__construct($response['items']);
    }

    public function current()
    {
        $item = parent::current();
        if (!isset($this->cache[$item['_id']])) {
            $this->cache[$item['_id']] = new FeedItem($item, $this->apiClient, $this);
        }
        return $this->cache[$item['_id']];
    }

    public function getSite($id)
    {
        return $this->sites[$id];
    }
    public function getFeed()
    {
        return $this->feed;
    }
}
