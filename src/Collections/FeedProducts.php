<?php

namespace StyleWishApi\Collections;

use ArrayIterator;
use StyleWishApi\Client as ApiClient;
use StyleWishApi\Objects\FeedProduct;

class FeedProducts extends ArrayIterator
{
    protected $apiClient;
    protected $sites;
    protected $feed;
    protected $cache = [];

    public function __construct(array $response, ApiClient $api)
    {
        $this->apiClient = $api;
        parent::__construct($response['products']);
    }

    public function current()
    {
        $item = parent::current();
        if (!isset($this->cache[$item['_id']])) {
            $this->cache[$item['_id']] = new FeedProduct($item, $this->apiClient, $this);
        }
        return $this->cache[$item['_id']];
    }
}
